const Room = require('../models/room')

exports.getRooms = async (req, res) => {
    const rooms = await Room.find({ users: { $elemMatch: { $eq: req.user._id  } } })
        .populate('users lastMessage')
        .sort([['lastUpdate', -1]])

    res.send(rooms)
}

exports.getRoomByIds = async (req, res) => {
    const ids = req.query.ids.split(',')
    const room = await Room.findOne({ $and: [ { users: { $elemMatch: { $eq: ids[0] } } }, { users: { $elemMatch: { $eq: ids[1] } } } ] })   

    res.send(room)
}