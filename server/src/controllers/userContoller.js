const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Subscription = require('../models/subscription')

exports.registerUser = async (req, res) => {
    const { username, email, password, firstName, secondName } = req.body

    const hashedPassword = await bcrypt.hash(password, 8)

    const user = new User({ username, email, firstName, secondName, password: hashedPassword })

    try {
        await user.save()

        res.status(201).send()
    } catch (e) {
        res.status(400).send()
    }
}

exports.loginUser = async (req, res) => {
    const { email, password } = req.body

    let user = await User.findOne({ email })

    if (!user || !await bcrypt.compare(password, user.password)) {
        return res.status(400).send({ error: 'Invalid credentials' })
    }

    const token = jwt.sign({ email }, process.env.JWT_SECRET)
    user = user.toObject()
    delete user.password

    res.send({ token, user })
}

exports.authUser = async (req, res, next) => {
    const token = req.headers.authorization.replace('Bearer ', '')

    if (token === 'null') {
        return res.status(401).send()
    }

    const payload = jwt.verify(token, process.env.JWT_SECRET)

    if (!payload) {
        return res.status(401).send()
    }

    const user = await User.findOne({ email: payload.email })

    if (!user) {
        return res.status(401).send()
    }

    req.user = user

    next()
}

exports.getMe = async (req, res) => {
    const token = req.headers.authorization.replace('Bearer ', '')

    if (token === 'null') {
        return res.status(401).send()
    }

    const payload = jwt.verify(token, process.env.JWT_SECRET)

    if (!payload) {
        return res.status(401).send()
    }

    let user = await User.findOne({ email: payload.email })

    if (!user) {
        return res.status(401).send()
    }

    const subscriptions = await Subscription.find({ subscriber: user._id })
        .populate('user')

    user = user.toObject()
    delete user.password

    res.send({...user, subscriptions})
}

exports.getUserProfile = async (req, res) => {
    const { userId } = req.params

    let user = await User.findById(userId)
    user = user.toObject()
    delete user.password

    res.send(user)
}

exports.getUsers = async (req, res) => {
    let users = await User.find({ username: { $regex: req.query.search }, _id: { $ne: req.user._id } })

    users = users.map((user) => {
        const objUser = user.toObject()
        delete objUser.password

        return objUser
    })

    res.send(users)
}