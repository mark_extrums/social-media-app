const Subscription = require('../models/subscription')

exports.getSubscriptions = async (req, res) => {

}

exports.createSubscription = async (req, res) => {
    const { _id: subscriber } = req.user
    const { userId } = req.body

    let subscription = new Subscription({ subscriber, user: userId })

    try {
        await subscription.save()
        subscription = subscription
            .populate('user')
        res.status(201).send(subscription)
    } catch (e) {
        res.status(400).send()
    }
}