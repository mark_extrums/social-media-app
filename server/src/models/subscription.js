const mongoose = require('mongoose')

const SubscriptionSchema = new mongoose.Schema({
    subscriber: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'user'
    }
})

module.exports = mongoose.model('subscription', SubscriptionSchema)