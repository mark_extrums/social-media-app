const mongoose = require('mongoose')

const MessageSchema = new mongoose.Schema({
    from: {
        type: mongoose.Types.ObjectId,
        ref: 'user',
        required: true
    },
    to: {
        type: mongoose.Types.ObjectId,
        ref: 'user',
        required: true
    },
    room: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'room'
    },
    content: {
        type: String,
        required: true
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('message', MessageSchema)