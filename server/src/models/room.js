const mongoose = require('mongoose')

const RoomSchema = new mongoose.Schema({
    users: [{
        type: mongoose.Types.ObjectId,
        ref: 'user'
    }],
    lastMessage: {
        type: mongoose.Types.ObjectId,
        ref: 'message'
    },
    active: {
        type: Boolean,
        default: false
    },
    lastUpdate: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('room', RoomSchema)