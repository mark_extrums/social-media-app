require('dotenv').config()
require('./db/mongoose')
const http = require('http')
const express = require('express')
const cors = require('cors')
const Room = require('./models/room')
const Message = require('./models/message')

const app = express()
const server = http.createServer(app)
const io = require('socket.io')(server)

app.use(cors({ origin: 'http://localhost:3000' }))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/users', require('./routers/user'))
app.use('/rooms', require('./routers/room'))
app.use('/subscriptions', require('./routers/subscription'))

io.on('connection', socket => {
    socket.on('Connect to the room', async (msg) => {
        let room 

        if (!msg.room) {
            room = await Room.findOne({ $and: [ { users: { $elemMatch: { $eq: msg.me } } }, { users: { $elemMatch: { $eq: msg.user } } } ] })    
        } else {
            room = await Room.findById(msg.room)
        }
        
        if (!room && !msg.room) {
            room = await new Room({ users: [msg.me, msg.user] }).save()
        } else if (!room && msg.room) {
            socket.emit('Connected to the room', { error: 'Cant find this room' })
        }

        const messages = await Message.find({ $or: [ { $and: [ { from: room.users[0] }, { to: room.users[1] } ] }, { $and: [ { from: room.users[1] }, { to: room.users[0] } ] } ] })
            .populate('from to')
            .sort('createdAt')

        socket.join(`${room._id}`)
        socket.emit('Connected to the room', { room: room._id, users: room.users, messages })
    })

    socket.on('New message from client', async (msg) => {
        const { from, to, content, room } = msg

        let message = new Message({ from, to, content, room })

        const promises = [
            message.save(),
            Room.findByIdAndUpdate(room, { lastUpdate: new Date(), lastMessage: message._id })
        ]

        const data = await Promise.all(promises)
            
        message = await data[0]
            .populate('from to')
            .execPopulate()

        io.sockets.in(room).emit('New message from server', { message })
    })

    socket.on('Disconnect from the room', msg => {
        socket.leave(msg.room)
    })
})

server.listen(process.env.PORT)