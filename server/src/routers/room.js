const express = require('express')
const { authUser } = require('../controllers/userContoller')
const { getRooms, getRoomByIds } = require('../controllers/roomsController')

const router = express.Router()

router.get('/', authUser, getRooms)
router.get('/room', authUser, getRoomByIds)

module.exports = router