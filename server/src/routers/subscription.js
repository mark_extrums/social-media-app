const express = require('express')
const { createSubscription } = require('../controllers/subscriptionsController')
const { authUser } = require('../controllers/userContoller')

const router = express.Router()

// router.get('/', authUser, createSubscription)
router.post('/', authUser, createSubscription)

module.exports = router