const express = require('express')
const { registerUser, loginUser, getMe, getUsers, authUser, getUserProfile } = require('../controllers/userContoller')

const router = express.Router()

router.post('/register', registerUser)
router.post('/login', loginUser)
router.get('/me', getMe)
router.get('/', authUser, getUsers)
router.get('/:userId', getUserProfile)

module.exports = router