import { call, put } from 'redux-saga/effects'
import { getProfileApi } from '../api'
import { putProfile } from '../actions/profileAction'

export function* getProfileWorker(action: any) {
    const userId = action.payload

    const { data } = yield call(getProfileApi, userId)
    yield put(putProfile(data))
}