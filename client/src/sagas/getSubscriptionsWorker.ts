import { call, put } from "redux-saga/effects";
import { getSubscriptionsApi } from "../api";
import { SEARCH_USERS_ACTION } from '../actions/types'
import { putSubscriptions } from "../actions/userActions";

export function* getSubscriptionsWorker(action: SEARCH_USERS_ACTION) {
    const userId = action.payload

    const { data } = yield call(getSubscriptionsApi, userId)

    yield put(putSubscriptions(data))
}