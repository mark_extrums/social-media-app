import { call, put } from "redux-saga/effects";
import { searchUsersApi } from "../api";
import { Action, SEARCH_USERS_ACTION } from '../actions/types'
import { putUsers } from "../actions/searchActions";

export function* searchUsersWorker(action: SEARCH_USERS_ACTION) {
    const query = action.payload

    const { data } = yield call(searchUsersApi, query)

    yield put(putUsers(data))
}