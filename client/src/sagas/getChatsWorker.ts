import { call, put } from 'redux-saga/effects'
import { getChatsApi } from '../api'
import { putChats } from '../actions/chatsActions'

export function* getChatsWorker(action: any) {
    const userId = action.payload

    const { data } = yield call(getChatsApi, userId)
    yield put(putChats(data))
}