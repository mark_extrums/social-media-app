import { call, put } from 'redux-saga/effects'
import { getMeApi } from '../api'
import { putUser } from '../actions/userActions'

export function* getUserWorker() {
    try {
        const { data } = yield call(getMeApi)
        yield put(putUser({ ...data, isAuth: true }))
    } catch (e) {
        yield put(putUser({ isAuth: false }))
    }
    
    
}