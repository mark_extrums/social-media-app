import { all, takeEvery } from "redux-saga/effects";
import { GET_ME, SEARCH_USERS, GET_CHATS, GET_PROFILE, CREATE_SUBSCRIPTION } from "../actions/types";
import { getUserWorker } from "./getUserWorker";
import { searchUsersWorker } from './searchUsersWorker'
import { getChatsWorker } from "./getChatsWorker";
import { getProfileWorker } from "./getProfileWorker";
import { createSubscriptionWorker } from "./createSubscriptionWorker";

export default function* root() {
    yield all([
        yield takeEvery(GET_ME, getUserWorker),
        yield takeEvery(SEARCH_USERS, searchUsersWorker),
        yield takeEvery(GET_CHATS, getChatsWorker),
        yield takeEvery(GET_PROFILE, getProfileWorker),
        yield takeEvery(CREATE_SUBSCRIPTION, createSubscriptionWorker)
    ])
}