import { call, put } from 'redux-saga/effects'
import { createSubscriptionApi } from '../api'
import { putSubscription } from '../actions/userActions'

export function* createSubscriptionWorker(action: any) {
    const userId = action.payload

    const { data } = yield call(createSubscriptionApi, userId)
    yield put(putSubscription(data))
}