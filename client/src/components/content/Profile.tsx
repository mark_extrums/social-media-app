import '../css/Profile.css'
import React, { useEffect, useContext } from 'react'
import AuthWrapper from '../authentication/AuthWrapper'
import { useParams, useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { getProfile } from '../../actions/profileAction'
import { UserInterface, createSubscription } from '../../actions/userActions'
import { getChatByIds } from '../../api'
import { ChatInterface } from '../../reducers/chatsReducer'
import SocketContext from '../context/socketContext'
import { putRoom, putMessages } from '../../actions/roomActions'

const Profile = () => {
    const params: { id: string } = useParams()
    const dispatch = useDispatch()
    const history = useHistory()
    const profile: UserInterface = useSelector((state: any) => state.profile)
    const me = useSelector((state: any) => state.user)
    const socket = useContext(SocketContext) as any

    useEffect(() => {
        dispatch(getProfile(params.id))
    }, [params.id])

    const onSendMessage = () => {
        socket.on('Connected to the room', (msg: any) => {
            dispatch(putRoom(msg.room, msg.users))
            dispatch(putMessages(msg.messages))
            history.push(`/chat/${msg.room}`)

            socket.removeEventListener('Connected to the room')
        })

        socket.emit('Connect to the room', { me: me._id, user: profile._id })
    }

    const onSubscribe = () => {
        dispatch(createSubscription(profile._id))
    }

    const renderButtons = () => {
        const match = me.subscriptions.find((sub: any) => sub.user._id === profile._id)
        if (me._id !== profile._id) {
            return (
                <>
                    <button 
                        onClick={onSendMessage}
                        className="send-msg-btn"
                    >Send message</button>
                    <button
                        onClick={onSubscribe}
                        disabled={match ? true : false}
                    >Subscribe</button>  
                </>
            )
        }
    }

    const renderProfile = () => {
        if (Object.keys(profile).length > 0 && me.isAuth !== null) {
            return (
                <div className="profile-block">
                    <h2>{profile.firstName} {profile.secondName}</h2>
                    <p>@{profile.username}</p>
                    <p>Email: {profile.email}</p>
                    {renderButtons()}
                </div>
            )
        } else {
            return null
        }
    }

    return (
        <AuthWrapper>
            <div className="profile">
                <h1>Profile</h1>
                {renderProfile()}
            </div>
        </AuthWrapper>
    )
}

export default Profile