import React from 'react'
import { ChatInterface } from '../../reducers/chatsReducer'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

interface ChatItemProps {
    chat: ChatInterface
}

const ChatItem = ({ chat }: ChatItemProps) => {
    const user = useSelector((state: any) => state.user)
    const history = useHistory()

    const renderChatItem = () => {
        if (user.isAuth === null || user.isAurh === false) {
            return null
        }

        let fromName, fromUsername, lastMessage = chat.lastMessage.content

        if (chat.users[0]._id === user._id) {
            fromName = `${chat.users[1].firstName} ${chat.users[1].secondName}`
            fromUsername = chat.users[1].username
        } else {
            fromName = `${chat.users[0].firstName} ${chat.users[0].secondName}`
            fromUsername = chat.users[0].username
        }

        if (lastMessage.length > 100) {
            lastMessage = chat.lastMessage.content.slice(0, 100) + '...'
        }

        return (
            <>
                <h3>{fromName}</h3>
                <p>@{fromUsername}</p>
                <p>{lastMessage}</p>
            </>
        )
    }

    const onChatItemClick = () => {
        history.push(`/chat/${chat._id}`)
    }

    return (
        <div 
            className="chat-item"
            onClick={onChatItemClick}
        >
            {renderChatItem()}
        </div>
    )
}

export default ChatItem