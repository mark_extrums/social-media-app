import React from 'react'
import { UserInterface } from '../../actions/userActions'
import UserItem from './UserItem'

interface UsersList {
    users: UserInterface[]
}

const UsersList = ({ users }: UsersList) => {
    return (
        <>
            {users.length ? users.map((user: UserInterface) => {
                return (
                    <UserItem 
                        key={user._id} 
                        firstName={user.firstName} 
                        secondName={user.secondName} 
                        username={user.username} 
                        id={user._id} 
                    />
                )
            }): null}
        </>
    )
}

export default UsersList