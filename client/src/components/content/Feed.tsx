import '../css/Feed.css'
import React from 'react'
import AuthWrapper from '../authentication/AuthWrapper'

const Feed = () => {
    return (
        <AuthWrapper>
            <div className="feed">
                <h1>Feed</h1>
            </div>
        </AuthWrapper>      
    )
}

export default Feed