import '../css/Messages.css'
import React, { useEffect } from 'react'
import AuthWrapper from '../authentication/AuthWrapper'
import { useSelector, useDispatch } from 'react-redux'
import { getChats } from '../../actions/chatsActions'
import { ChatInterface } from '../../reducers/chatsReducer'
import ChatItem from './ChatItem'

const Messages = () => {
    const user = useSelector((state: any) => state.user)
    const chats = useSelector((state: any) => state.chats)
    const dispatch = useDispatch()

    const renderMessages = () => {
        if (user.isAuth !== null && user.isAuth === false && chats.length) {
            return chats.map((chat: ChatInterface) => {
                return <ChatItem chat={chat} key={chat._id} />
            })
        }
    }

    useEffect(() => {
        if (user !== null && Object.keys(user).length) {
            dispatch(getChats(user._id))
        }
    }, [user])

    return (
        <AuthWrapper>
            <div className="messages">
                <h1>Messages</h1>
                {renderMessages()}
            </div>  
        </AuthWrapper>         
    )
}

export default Messages