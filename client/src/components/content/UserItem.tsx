import React, { useContext } from 'react'
import SocketContext from '../context/socketContext'
import { useDispatch, useSelector } from 'react-redux'
import { putRoom, putMessages } from '../../actions/roomActions'
import { useHistory } from 'react-router-dom'

export interface UserItemProps {
    firstName: string
    secondName: string
    username: string
    id: string
}

const UserItem = ({ firstName, secondName, username, id }: UserItemProps) => {
    const socket = useContext(SocketContext) as any
    const userId = useSelector((state: any) => state.user._id)
    const dispatch = useDispatch()
    const history = useHistory()

    const onSendMessage = () => {
        socket.on('Connected to the room', (msg: any) => {
            dispatch(putRoom(msg.room, msg.users))
            dispatch(putMessages(msg.messages))
            history.push(`/chat/${msg.room}`)

            socket.removeEventListener('Connected to the room')
        })

        socket.emit('Connect to the room', { me: userId, user: id })
    }

    const onItemClick = () => {
        history.push(`/profile/${id}`)
    }

    return (
        <div className="user-item">
            <div className="left-block">
                <h3 onClick={onItemClick}>{firstName} {secondName}</h3>
                <p onClick={onItemClick}>@{username}</p>
            </div>
            <div className="right-block">
                <button
                    onClick={onSendMessage}
                    className="send-msg-btn"
                >Send message</button>
            </div>
        </div>
    )
}

export default UserItem