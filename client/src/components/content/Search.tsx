import '../css/Search.css'
import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { searchUsers } from '../../actions/searchActions'
import { UserInterface } from '../../actions/userActions'
import UserItem from './UserItem'
import AuthWrapper from '../authentication/AuthWrapper'
import UsersList from './UsersList'

const Search = () => {
    const users = useSelector((state: any) => state.search)
    const dispatch = useDispatch()
    const [input, setInput] = useState('')

    useEffect(() => {
        dispatch(searchUsers(input))
    }, [input])

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInput(e.target.value)
    }

    return (
        <AuthWrapper>
            <div className="search">
                <h1>Search</h1>
                <input 
                    value={input} 
                    onChange={onInputChange} 
                    type="text"
                    className="search-input"
                    placeholder="Search query..."
                />
                <div className="content">
                    <UsersList users={users} />
                </div>
            </div>
        </AuthWrapper>
    )
}

export default Search