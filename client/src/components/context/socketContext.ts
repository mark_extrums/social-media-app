import React from 'react'

const SocketContext = React.createContext(null) as any

export default SocketContext