import './css/App.css'
import React, { useEffect, useState } from 'react'
import io from 'socket.io-client'
import { BrowserRouter, Route, useHistory } from 'react-router-dom'
import Register from './authentication/Register'
import Login from './authentication/Login'
import { useDispatch, useSelector } from 'react-redux'
import { getMe } from '../actions/userActions'
import SocketContext from './context/socketContext'
import Chat from './chat/Chat'
import LeftSideMenu from './staticComonents/LeftSideMenu'
import Feed from './content/Feed'
import Messages from './content/Messages'
import Profile from './content/Profile'
import Search from './content/Search'
import Main from './staticComonents/Main'

const socket = io('http://localhost:4000')

const App = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getMe())   
    }, [])

    return (
        <div className="container">
            <SocketContext.Provider value={socket}>
                <BrowserRouter>
                    <LeftSideMenu />
                    <Route component={Register} path="/register" exact />
                    <Route component={Login} path="/login" exact />
                    <Route component={Main} path="/" exact />
                    <Route component={Chat} path="/chat/:id" exact />
                    <Route component={Feed} path="/feed" exact />
                    <Route component={Messages} path="/messages" exact />
                    <Route component={Profile} path="/profile/:id" exact />
                    <Route component={Search} path="/search" exact />
                </BrowserRouter>
            </SocketContext.Provider>
        </div>
    )
}

export default App