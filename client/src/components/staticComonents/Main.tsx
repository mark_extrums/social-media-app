import React from 'react'
import { useHistory } from 'react-router-dom'

const Main = () => {
    const history = useHistory()

    return (
        <>
            {history.push('/feed')}
        </>
    )
}

export default Main