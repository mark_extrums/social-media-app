import '../css/LeftSideMenu.css'
import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { putUser } from '../../actions/userActions'

const LeftSideMenu = () => {
    const user = useSelector((state: any) => state.user)
    const dispatch = useDispatch()

    const onLogOut = () => {
        localStorage.removeItem('jwt')
        dispatch(putUser({}))
    }

    const renderMenu = () => {
        if (user === null) {
            return null
        } else if (!Object.keys(user).length) {
            return null
        } else {
            return (
                <div className="left-side-menu">
                    <Link className="item" to="/feed">Feed</Link>
                    <Link className="item" to={`/profile/${user._id}`}>Profile</Link>
                    <Link className="item" to="/messages">Messages</Link>
                    <Link className="item" to="/subscriptions">Subscriptions</Link>
                    <Link className="item" to="/search">Search</Link>
                    <p 
                        className="item"
                        onClick={onLogOut}
                    >Logout</p>
                </div>
            )
        }
    }

    return (
        <>
            {renderMenu()}
        </>  
    )
}

export default LeftSideMenu

