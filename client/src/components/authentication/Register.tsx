import '../css/Form.css'
import React from 'react'
import { Formik, Form, Field, FormikHelpers } from 'formik'
import * as Yup from 'yup'
import { Link, useHistory } from 'react-router-dom'
import { registerUserApi } from '../../api'

export interface RegisterFormValues {
    email: string
    password: string
    username: string
    firstName: string
    secondName: string
}

const Register = () => {
    const history = useHistory()

    const ValidationSchema = Yup.object().shape({
        email: Yup.string()
            .required('Email is required')
            .email('Invalid email'),
        password: Yup.string()
            .required('Password is required')
            .min(8, 'Password should be >= 8 symbols'),
        username: Yup.string()
            .required('Username is required')
            .min(4, 'Username should be >= 4 symbols'),
        firstName: Yup.string()
            .required('Firstname is required'),
        secondName: Yup.string()
            .required('Second name is required')
        
    })

    const onSubmit = async (values: RegisterFormValues, { setSubmitting, setFieldError }: FormikHelpers<RegisterFormValues>) => {
        try {
            setSubmitting(false)
            await registerUserApi(values)

            history.push('/login')
        } catch (e) {
            // in the future also add username validation
            setFieldError('email', 'Email is already in use')
        }
    }

    return (
        <Formik
            initialValues={{
                email: '',
                password: '',
                username: '',
                firstName: '',
                secondName: ''
            }}
            onSubmit={onSubmit}
            validationSchema={ValidationSchema}
        >
            {({ errors, touched }) => (
                <Form className="auth-form">
                    <label htmlFor="firtName">First name</label>
                    <Field id="firstName" name="firstName" type="firstName" className="auth-input" />
                    {errors.firstName && touched.firstName ? (
                        <div className="input-error">{errors.firstName}</div>
                    ) : null}

                    <label htmlFor="secondName">Second name</label>
                    <Field id="secondName" name="secondName" type="secondName" className="auth-input" />
                    {errors.secondName && touched.secondName ? (
                        <div className="input-error">{errors.secondName}</div>
                    ) : null}

                    <label htmlFor="username">Username</label>
                    <Field id="username" name="username" type="username" className="auth-input" />
                    {errors.username && touched.username ? (
                        <div className="input-error">{errors.username}</div>
                    ) : null}

                    <label htmlFor="email">Email</label>
                    <Field id="email" name="email" type="email" className="auth-input" />
                    {errors.email && touched.email ? (
                        <div className="input-error">{errors.email}</div>
                    ) : null}

                    <label htmlFor="password">Password</label>
                    <Field id="password" name="password" type="password" className="auth-input" />
                    {errors.password && touched.password ? (
                        <div className="input-error">{errors.password}</div>
                    ) : null}
                    
                    <div className="buttons-block">
                        <button className="submit-btn" type="submit">Register</button>
                        <Link to="/login" className="auth-link">Login</Link>
                    </div>
                </Form>
            )}
                
        </Formik>
    )
}

export default Register