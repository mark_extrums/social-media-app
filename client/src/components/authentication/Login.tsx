import '../css/Form.css'
import React from 'react'
import { Formik, Form, Field, FormikHelpers } from 'formik'
import * as Yup from 'yup'
import { Link, useHistory } from 'react-router-dom'
import { registerUserApi, loginUserApi } from '../../api'
import { useDispatch } from 'react-redux'
import { putUser } from '../../actions/userActions'

export interface LoginFormValues {
    email: string
    password: string
}

const Login = () => {
    const history = useHistory()
    const dispatch = useDispatch()

    const ValidationSchema = Yup.object().shape({
        email: Yup.string()
            .required('Email is required')
            .email('Invalid email'),
        password: Yup.string()
            .required('Password is required')
            .min(8, 'Password should be >= 8 symbols'),
        
    })

    const onSubmit = async (values: LoginFormValues, { setSubmitting, setFieldError }: FormikHelpers<LoginFormValues>) => {
        try {
            setSubmitting(false)
            const {data} = await loginUserApi(values)

            localStorage.setItem('jwt', data.token)
            dispatch(putUser(data.user))

            history.push('/')
        } catch (e) {
            // in the future also add username validation
            setFieldError('email', 'Email is already in use')
        }
    }

    return (
        <Formik
            initialValues={{
                email: '',
                password: ''
            }}
            onSubmit={onSubmit}
            validationSchema={ValidationSchema}
        >
            {({ errors, touched }) => (
                <Form className="auth-form">
                    <label htmlFor="email">Email</label>
                    <Field id="email" name="email" type="email" className="auth-input" />
                    {errors.email && touched.email ? (
                        <div className="input-error">{errors.email}</div>
                    ) : null}

                    <label htmlFor="password">Password</label>
                    <Field id="password" name="password" type="password" className="auth-input" />
                    {errors.password && touched.password ? (
                        <div className="input-error">{errors.password}</div>
                    ) : null}
                    
                    <div className="buttons-block">
                        <button className="submit-btn" type="submit">Login</button>
                        <Link to="/register" className="auth-link">Register</Link>
                    </div>
                </Form>
            )}
                
        </Formik>
    )
}

export default Login