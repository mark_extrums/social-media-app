import React, { ReactNode } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

interface AuthWrapperProps {
    children: ReactNode
}

const AuthWrapper = ({ children }: AuthWrapperProps) => {
    const user = useSelector((state: any) => state.user)
    const history = useHistory()

    const renderPage = () => {
        if (user.isAuth === null) {
            return (
                <div>
                    <p></p>
                    <div className="ui active inverted dimmer">
                        <div className="ui text loader">Loading...</div>
                    </div>
                </div>
            )
        } else if (user.isAuth === false) {
            history.push('/login')
        } else {
            return children
        }
    }

    return (
        <>
            {renderPage()}
        </>
    )
}

export default AuthWrapper