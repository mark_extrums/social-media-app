import '../css/Message.css'
import React from 'react'
import { UserInterface } from '../../actions/userActions'
import { useSelector } from 'react-redux'

interface MessageProps {
    createdAt: Date,
    content: string,
    from: UserInterface
}

const Message = ({ createdAt, content, from }: MessageProps) => {
    const me = useSelector((state: any) => state.user._id)

    return (
        <div className={me === from._id ? 'my-message message' : 'user-message message'}>
            <h5>{from.firstName}</h5>
            <p>{content}</p>
            <p>{createdAt}</p>
        </div>
    )
}

export default Message