import '../css/Chat.css'
import React, { useState, useContext, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { MessageInterface } from '../../reducers/roomReducer'
import Message from './Message'
import SocketContext from '../context/socketContext'
import { putMessage, putRoom, putMessages, clearRoom } from '../../actions/roomActions'
import { useHistory, useParams } from 'react-router-dom'
import Scrollbars from 'react-custom-scrollbars'
import AuthWrapper from '../authentication/AuthWrapper'

const Chat = () => {
    const messages = useSelector((state: any) => state.room.messages)
    const room = useSelector((state: any) => state.room.roomId)
    const users = useSelector((state: any) => state.room.users)
    const me = useSelector((state: any) => state.user)

    const socket = useContext(SocketContext) as any
    const [input, setInput] = useState('')
    const dispatch = useDispatch()
    const params: { id: string } = useParams()
    const history = useHistory()
    const scrollbar = useRef(null) as any

    const onInputChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setInput(e.target.value)
    } 

    const onMessageSend = () => {
        socket.emit('New message from client', { 
            room,
            from: me._id,
            to: me._id === users[0] ? users[1] : users[0],
            content: input
        })
    }

    useEffect(() => {
        if (messages.length > 0) {
            scrollbar.current.scrollToBottom()
        }
    }, [messages])

    useEffect(() => {
        if (!room) {
            socket.emit('Connect to the room', { room: params.id })
            socket.on('Connected to the room', (msg: any) => {
                socket.removeEventListener('Connected to the room')
                if (msg.error) {
                    history.push('/eror')
                    return
                }

                dispatch(putRoom(msg.room, msg.users))
                dispatch(putMessages(msg.messages))
            })
        }

        socket.on('New message from server', (msg: any) => {
            dispatch(putMessage(msg.message))
        })

        return function cleanup() {
            socket.removeEventListener('New message from server')
            dispatch(clearRoom())
        }
    }, [])

    return (
        <AuthWrapper>
             <div className="chat">
                <Scrollbars 
                    style={{ height: 500}} 
                    ref={scrollbar}
                >
                    <div className="chat-messages">
                        {room && messages.length ? messages.map((message: MessageInterface) => {
                            const { createdAt, content, from } = message

                            return (
                                <Message 
                                    createdAt={createdAt} 
                                    content={content} 
                                    from={from} 
                                    key={message._id} 
                                />
                            )
                        }) : null}
                    </div>
                    
                </Scrollbars>
                <form className="chat-form"
                    onSubmit={(e: React.FormEvent) => {
                        e.preventDefault()
                        onMessageSend()
                    }}
                >
                    <textarea 
                        value={input} 
                        onChange={onInputChange}
                        className="message-input"
                    />
                    <button className="send-message-btn" type="submit">Send</button>
                </form>
            
            </div>
        </AuthWrapper>      
    )
}

export default Chat