import { UserInterface } from "./userActions"
import { MessageInterface } from "../reducers/roomReducer"
import { ChatInterface } from '../reducers/chatsReducer'

export const PUT_USER = 'PUT_USER'
export const GET_ME = 'GET_ME'
export const PUT_ROOM = 'PUT_ROOM'
export const PUT_MESSAGES = 'PUT_MESSAGES'
export const PUT_MESSAGE = 'PUT_MESSAGE'
export const SEARCH_USERS = 'SEARCH_USERS'
export const PUT_USERS = 'PUT_USERS'
export const GET_CHATS = 'GET_CHATS'
export const PUT_CHATS = 'PUT_CHATS'
export const GET_PROFILE = 'GET_PROFILE'
export const PUT_PROFILE = 'PUT_PROFILE'
export const CLEAR_ROOM = 'CLEAR_ROOM'
export const GET_SUBSCRIPTIONS = 'GET_SUBSCRIPTIONS'
export const PUT_SUBSCRIPTIONS = 'PUT_SUBSCRIPTIONS'
export const CREATE_SUBSCRIPTION = 'CREATE_SUBSCRIPTION'
export const PUT_SUBSCRIPTION = 'PUT_SUBSCRIPTION'

interface PUT_USER_ACTION {
    type: typeof PUT_USER
    payload: UserInterface | {}
}

interface GET_ME_ACTION {
    type: typeof GET_ME
}

interface PUT_ROOM_ACTION {
    type: typeof PUT_ROOM,
    payload: {
        room: string,
        users: string[]
    }
}

interface PUT_MESSAGES_ACTION {
    type: typeof PUT_MESSAGES,
    payload: MessageInterface[]
}

interface PUT_MESSAGE_ACTION {
    type: typeof PUT_MESSAGE,
    payload: MessageInterface
}

interface PUT_USERS {
    type: typeof PUT_USERS,
    payload: UserInterface[]
}

interface GET_CHATS_ACTION {
    type: typeof GET_CHATS,
    payload: string
}

interface PUT_CHATS_ACTION {
    type: typeof PUT_CHATS,
    payload: ChatInterface[]
}

interface GET_PROFILE_ACTION {
    type: typeof GET_PROFILE,
    payload: string
}

interface PUT_PROFILE_ACTION {
    type: typeof PUT_PROFILE,
    payload: UserInterface
}

interface CLEARE_ROOM_ACTION {
    type: typeof CLEAR_ROOM
}

interface GET_SUBSCRIPTIONS_ACTION {
    type: typeof GET_SUBSCRIPTIONS,
    payload: string
}

interface PUT_SUBSCRIPTIONS_ACTION {
    type: typeof PUT_SUBSCRIPTIONS,
    payload: UserInterface[]
}

interface CREATE_SUBSCRIPTION_ACTION {
    type: typeof CREATE_SUBSCRIPTION,
    payload: string
}

interface PUT_SUBSCRIPTION_ACTION {
    type: typeof PUT_SUBSCRIPTION,
    payload: UserInterface[]
}

export interface SEARCH_USERS_ACTION {
    type: typeof SEARCH_USERS,
    payload: string
}

export type Action = PUT_USER_ACTION | GET_ME_ACTION | PUT_ROOM_ACTION | PUT_MESSAGES_ACTION | PUT_MESSAGE_ACTION | SEARCH_USERS_ACTION | PUT_USERS | PUT_CHATS_ACTION | PUT_PROFILE_ACTION | CLEARE_ROOM_ACTION | PUT_SUBSCRIPTIONS_ACTION | PUT_SUBSCRIPTION_ACTION