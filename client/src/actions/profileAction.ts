import { GET_PROFILE, PUT_PROFILE } from "./types"
import { UserInterface } from "./userActions"

export const getProfile = (id: string) => {
    return {
        type: GET_PROFILE,
        payload: id
    }
}

export const putProfile = (user: UserInterface) => {
    return {
        type: PUT_PROFILE,
        payload: user
    }
}