import { GET_CHATS, PUT_CHATS } from "./types"
import { ChatInterface } from "../reducers/chatsReducer"

export const getChats = (userId: string) => {
    return {
        type: GET_CHATS,
        payload: userId
    }
}

export const putChats = (chats: ChatInterface[]) => {
    return {
        type: PUT_CHATS,
        payload: chats
    }
}