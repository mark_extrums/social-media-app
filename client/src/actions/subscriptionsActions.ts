import { GET_SUBSCRIPTIONS } from "./types"
import { UserInterface } from "./userActions"

export const getSubscriptions = (userId: string) => {
    return {
        type: GET_SUBSCRIPTIONS,
        payload: userId
    }
}