import { SEARCH_USERS, PUT_USERS } from "./types"
import { UserInterface } from "./userActions"

export const searchUsers = (query: string) => {
    return {
        type: SEARCH_USERS,
        payload: query
    }
}

export const putUsers = (users: UserInterface[]) => {
    return {
        type: PUT_USERS,
        payload: users
    }
}