import { PUT_ROOM, PUT_MESSAGES, PUT_MESSAGE, PUT_PROFILE, CLEAR_ROOM } from "./types"
import { MessageInterface } from "../reducers/roomReducer"

export const putRoom = (id: string, users: string[]) => {
    return {
        type: PUT_ROOM,
        payload: {
            room: id,
            users
        }
    }
}

export const putMessages = (messages: MessageInterface[]) => {
    return {
        type: PUT_MESSAGES,
        payload: messages
    }
}

export const putMessage = (message: MessageInterface) => {
    return {
        type: PUT_MESSAGE,
        payload: message
    }
}

export const clearRoom = () => {
    return {
        type: CLEAR_ROOM
    }
}