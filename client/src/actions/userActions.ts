import { Action, PUT_USER, GET_ME, PUT_SUBSCRIPTIONS, PUT_SUBSCRIPTION, CREATE_SUBSCRIPTION } from "./types"

export interface UserInterface {
    _id: string
    firstName: string
    secondName: string
    email: string
    username: string,
    subscriptions: UserInterface[],
    isAuth: boolean | null
}

export const putUser = (user: UserInterface | {}): Action => {
    return {
        type: PUT_USER,
        payload: user
    }
}

export const getMe = (): Action => {
    return {
        type: GET_ME
    }
}

export const putSubscriptions = (subscriptions: UserInterface[]) => {
    return {
        type: PUT_SUBSCRIPTIONS,
        payload: subscriptions
    }
}

export const putSubscription = (subscription: UserInterface) => {
    return {
        type: PUT_SUBSCRIPTION,
        payload: subscription
    }
}

export const createSubscription = (userId: string) => {
    return {
        type: CREATE_SUBSCRIPTION,
        payload: userId
    }
}