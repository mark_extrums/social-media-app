import { UserInterface } from "../actions/userActions"
import { Action, PUT_PROFILE } from "../actions/types"


const profileReducer = (state: Partial<UserInterface> = {}, action: Action) => {
    switch(action.type) {
        case PUT_PROFILE:
            return { ...action.payload }
        default:
            return state
    }
}

export default profileReducer