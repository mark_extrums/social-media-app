import { UserInterface } from "../actions/userActions"
import { Action, PUT_CHATS } from "../actions/types"
import { MessageInterface } from "./roomReducer"

export interface ChatInterface {
    _id: string
    users: UserInterface[]
    lastMessage: MessageInterface
}

const chatsReducer = (state: ChatInterface[] = [], action: Action) => {
    switch (action.type) {
        case PUT_CHATS:
            return [...action.payload]
        default:
            return state
    }
}   

export default chatsReducer