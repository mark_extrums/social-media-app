import { Action, PUT_ROOM, PUT_MESSAGES, PUT_MESSAGE, CLEAR_ROOM } from "../actions/types"
import { UserInterface } from "../actions/userActions"

export interface MessageInterface {
    from: UserInterface
    to: UserInterface
    room: string
    _id: string
    createdAt: Date
    content: string
}

export interface RoomInterface {
    roomId: string
    users: Array<string>
    messages: MessageInterface[]
}

const roomReducer = (state: RoomInterface = {
    roomId: '',
    users: [],
    messages: []
}, action: Action) => {
    switch(action.type) {
        case PUT_ROOM:
            return { ...state, roomId: action.payload.room, users: action.payload.users }
        case PUT_MESSAGES:
            return { ...state, messages: action.payload }
        case PUT_MESSAGE:
            return { ...state, messages: [ ...state.messages, action.payload ] }
        case CLEAR_ROOM:
            return { roomId: '', users: [], messages: [] }
        default:
            return state
    }
}

export default roomReducer