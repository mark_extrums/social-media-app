import { UserInterface } from "../actions/userActions"
import { Action, PUT_SUBSCRIPTIONS, PUT_SUBSCRIPTION } from "../actions/types"


const subscriptionsReducer = (state: UserInterface[] = [], action: Action) => {
    switch(action.type) {
        case PUT_SUBSCRIPTIONS:
            return [ ...action.payload ]
        default:
            return state
    }
}

export default subscriptionsReducer