import { combineReducers } from 'redux'
import userReducer from './userReducer'
import roomReducer from './roomReducer'
import searchReducer from './searchReducer'
import chatsReducer from './chatsReducer'
import profileReducer from './profileReducer'
import subscriptionsReducer from './subscriptionsReducer'

export default combineReducers({
    user: userReducer,
    room: roomReducer,
    search: searchReducer,
    chats: chatsReducer,
    profile: profileReducer,
    subscriptions: subscriptionsReducer
})