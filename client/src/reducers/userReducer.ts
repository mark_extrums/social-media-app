import { UserInterface } from "../actions/userActions";
import { Action, PUT_USER, PUT_SUBSCRIPTION } from "../actions/types";

const userReducer = (state: UserInterface = {
    _id: '',
    firstName: '',
    secondName: '',
    email: '',
    username: '',
    subscriptions: [],
    isAuth: null
}, action: Action) => {
    switch (action.type) {
        case PUT_USER:
            return { ...state, ...action.payload }
        case PUT_SUBSCRIPTION:
            return { ...state, subscriptions: [ ...state.subscriptions, action.payload ] }
        default: 
            return state
    }
}

export default userReducer