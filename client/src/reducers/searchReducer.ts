import { UserInterface } from "../actions/userActions"
import { Action, PUT_USERS } from "../actions/types"

const searchReducer = (state: UserInterface[] = [], action: Action) => {
    switch(action.type) {
        case PUT_USERS:
            return [ ...action.payload ]
        default:
            return state
    }
}

export default searchReducer