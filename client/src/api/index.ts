import Axios, { AxiosResponse } from 'axios'
import { RegisterFormValues } from "../components/authentication/Register";
import { LoginFormValues } from '../components/authentication/Login';
import { ChatInterface } from '../reducers/chatsReducer';

const baseUrl: string = 'http://localhost:4000'

export const registerUserApi = async (values: RegisterFormValues) => {
    return await Axios({
        url: `${baseUrl}/users/register`,
        method: 'POST',
        data: {
            ...values
        }
    })
}

export const loginUserApi = async (values: LoginFormValues) => {
    return await Axios({
        url: `${baseUrl}/users/login`,
        method: 'POST',
        data: {
            ...values
        }
    })
}

export const getMeApi = async () => {
    return Axios({
        url: `${baseUrl}/users/me`,
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    })
}

export const searchUsersApi = async (query: string) => {
    return Axios({
        url: `${baseUrl}/users?search=${query}`,
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    })
}

export const getChatsApi = async (userId: string) => {
    return Axios({
        url: `${baseUrl}/rooms`,
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        },
        data: {
            userId
        }
    })
}

export const getProfileApi = async (userId: string) => {
    return Axios({
        url: `${baseUrl}/users/${userId}`,
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    })
}

export const getChatByIds = async (ids: Array<string>) => {
    return await Axios({
        url: `${baseUrl}/rooms/room?ids=${ids[0]},${ids[1]}`,
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    })
}

export const getSubscriptionsApi = (userId: string) => {
    return Axios({
        url: `${baseUrl}/subscriptions/${userId}`
    })
}

export const createSubscriptionApi = (userId: string) => {
    return Axios({
        url: `${baseUrl}/subscriptions`,
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        },
        data: {
            userId
        }
    })
}